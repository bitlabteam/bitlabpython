import struct #allows you to pack information in a specific way
import time
import random
import socket
import codecs
import hashlib
import numpy as np

globalNumberOfBlocks = 0
class headerClass:
    magic = b'\0' * 4
    command = b'\0' * 12
    length = b'\0' * 4
    checksum = b'\0' * 4

class massageClass:
    header = headerClass()
    payload = b'\0'

def readHeader(s):
    header = s.recv(24)
    return header

#def little_endian_hex_to_big_endian_hex(littleEndian):
#   bigEndian = b'\0' * len(littleEndian)
    #range(len(littleEndian))
    #bigEndian[i] = littleEndian[-(i-1)]
#    for i in range(len(littleEndian)):
#        bigEndian[i] = littleEndian[-(i-1)]
#    return bigEndian

def headerInterpreter(input):
    print("header interpreter")
    #print("input")
    #print(input.hex())
    h = headerClass()
    #print("header class default object")
    #print("magic: " + h.magic.hex())
    h.magic = input[:4]
    #print("magic after change")
    #print(h.magic.hex())
    print("magic: " + h.magic.hex())
    #print("command")
    #print(h.command.hex())
    h.command = input[4:16]
    #print("command after change")
    #print(h.command.hex())
    command_actual_length = 0
    for i in h.command:
        if(i != 0):
            command_actual_length += 1
        else:
            break
    #print("command_actual_length")
    #print(command_actual_length)
    command_in_str = h.command[:command_actual_length].decode("utf-8")
    h.command = command_in_str
    #print("command_in_str")
    print("command: " + str(h.command))
    h.length = input[16:20]
    #print("length in hex")
    print(h.length.hex())
    #h.length = little_endian_hex_to_big_endian_hex(h.length)
    #print("length big endian")
    #print(h.length.hex())
    h.length = int.from_bytes(h.length, byteorder='little')
    print("payload length: " + str(h.length))
    #h.length = int(h.length.hex(), 16)
    #print(h.length)
    h.checksum = input[20:]
    print("checksum: " + h.checksum.hex())
    return h

def compactSizeUintInterpreter(firstBit):
    iterationStart = -1
    iterationEnd = -1
    if(firstBit == 253): #0xfd
            print("type uint16_t")
            iterationStart = 1
            iterationEnd = 3
    else:
            if(firstBit == 254): #0xfe
                print("type uint32_t")
                iterationStart = 1
                iterationEnd = 5
            else:
                if(firstBit == 255): #0xff
                    print("type uint64_t")
                    iterationStart = 1
                    iterationEnd = 9
                else:
                    print("type uint8_t")
                    iterationStart = 0
                    iterationEnd = 1
    
    iteration = [iterationStart, iterationEnd]
    return iteration

def blockTypeIdentifier(typeIdentifier):
    if(typeIdentifier == 1):
        return "MSG_TX"
    if(typeIdentifier == 2):
        return "MSG_BLOCK"
    if(typeIdentifier == 3):
        return "MSG_FILTERED_BLOCK"
    if(typeIdentifier == 4):
        return "MSG_CMPCT_BLOCK"
    if(typeIdentifier == 5):
        return "MSG_WITNESS_BLOCK"
    if(typeIdentifier == 6):
        return "MSG_WITNESS_TX"
    if(typeIdentifier == 7):
        return "MSG_FILTERED_WITNESS_BLOCK"
myBlockHeaders = []
myBlockHeadersHashes = []
myBlockHeadersHashesInBytes = []
myBlocks = []
def payloadInterpreter(payload, commandName):
    print("payload interpreter")
    payloadStr = []
    if(commandName == 'addr'):
        #numberOfAddresses = payload[0]
        #iterationStart = 0
        #if numberOfAddresses * 30 == len(payload)-1:
        #    iterationStart = 1
        #else:
        #    numberOfAddresses = payload[:2]
        #    if numberOfAddresses * 30 == len(payload)-2:
        #        iterationStart = 2
        #    else:
        #        print("payloadInterpreter error")
        #        return -1
        #payloadStr.append("Number of addresses:" + str(numberOfAddresses))
        #for i in range(numberOfAddresses):
        #numberOfAddresses = len(payload) // 30 #// is floor division
        #bitsOfNumberOfAddresses = len(payload) % 30
        #print("number of addresses")
        #print(numberOfAddresses)
        #numberOfAddressesFromBits = payload[:bitsOfNumberOfAddresses]
        #print("number of according to bits")
        #print(int(numberOfAddressesFromBits.hex(), 16))
        #if numberOfAddresses != int(numberOfAddressesFromBits.hex(), 16):
        #    print("payloadInterpreter error")
        #    return -1
        #else:
        #    payloadStr.append("Number of addresses: " + str(numberOfAddresses))
        firstBit = payload[0]
        #iterationStart = -1
        #iterationEnd = -1
        iteration = compactSizeUintInterpreter(firstBit)
        numberOfAddresses = int.from_bytes(payload[iteration[0]:iteration[1]], byteorder='little')
        #print("Number of addresses")
        #print(numberOfAddresses)
        payloadStr.append("Number of addresses: " + str(numberOfAddresses))
        addressBegin = iteration[1]
        addressEnd = iteration[1] + 30
        while addressEnd <= len(payload):
            address = payload[addressBegin:addressEnd]
            #payloadStr.append("Address \n" + address.hex())
            payloadStr.append("Address \n" +
                              "time: " + time.strftime('%Y-%m-%d %H:%M:%S',
                                                       time.gmtime(int.from_bytes(address[0:4], byteorder='little'))) + ", " +
                              "services: " + str(int.from_bytes(address[4:12], byteorder='little')) + ", " +
                              "IP address: " +
                              address[12:14].hex() + ":" +
                              address[14:16].hex() + ":" +
                              address[16:18].hex() + ":" +
                              address[18:20].hex() + ":" +
                              address[20:22].hex() + ":" +
                              address[22:24].hex() + ":" +
                              address[24:26].hex() + ":" +
                              address[26:28].hex() + ":" + ", " +
                              "Port: " + str(int.from_bytes(address[28:30], byteorder='big'))
                              )
            addressBegin = addressEnd
            addressEnd += 30
    else:
        if(commandName == 'inv'):
            firstBit = payload[0]
            #iterationStart = -1
            #iterationEnd = -1
            iteration = compactSizeUintInterpreter(firstBit)
            numberOfBlocks = int.from_bytes(payload[iteration[0]:iteration[1]], byteorder='little')
            #print("Number of blocks")
            #print(numberOfBlocks)
            payloadStr.append("Number of blocks: " + str(numberOfBlocks))
            blockBegin = iteration[1]
            blockLength = 36
            blockEnd = iteration[1] + blockLength
            for i in range(numberOfBlocks):
                block = payload[blockBegin:blockEnd]
                myBlockHeadersHashesInBytes.append(block)
                payloadStr.append("Block\n" +
                                  "Type: " + blockTypeIdentifier(int.from_bytes(block[0:4], byteorder='little')) + "\n" +
                                  "Hash: \n" + block[4:36].hex()
                                  )
                blockBegin = blockEnd
                blockEnd += blockLength
        else:
                if(commandName == 'headers'):
                    firstBit = payload[0]
                    iteration = compactSizeUintInterpreter(firstBit)
                    numberOfHeaders = int.from_bytes(payload[iteration[0]:iteration[1]], byteorder='little')
                    #print(numberOfHeaders)
                    payloadStr.append("Number of headers: " + str(numberOfHeaders))
                    headerBegin = iteration[1]
                    headerLength = 80
                    headerEnd = iteration[1] + headerLength
                    for i in range(numberOfHeaders):
                        header = payload[headerBegin:headerEnd]
                        #new

                        #myBlockHeadersHashesInBytes.append(hashlib.sha256(hashlib.sha256(header).digest()).digest()[:4])
                        data = "Header\n" +\
                                          "Version: " + str(int.from_bytes(header[0:4], byteorder='little')) + ", " +\
                                          "Previous block header hash: " + header[4:36].hex() + ", " +\
                                          "Merkle root hash: " + header[36:68].hex() + ", " +\
                                          "Time: " + time.strftime('%Y-%m-%d %H:%M:%S',
                                                       time.gmtime(int.from_bytes(header[68:72], byteorder='little'))) + ", " +\
                                          "nBits: " + str(int.from_bytes(header[72:76], byteorder='little')) + ", " +\
                                          "Nonce: " + str(int.from_bytes(header[76:80], byteorder='little'))
                        payloadStr.append(data)
                        myBlockHeaders.append(data)
                        headerBegin = headerEnd
                        headerEnd += headerLength
                else:
                    if(commandName == 'block'):
                        global globalNumberOfBlocks
                        header = payload[0:80]
                        headerStr = "Header\n" + \
                                          "Version: " + str(int.from_bytes(header[0:4], byteorder='little')) + ", " + \
                                          "Previous block header hash: " + header[4:36].hex() + ", " + \
                                          "Merkle root hash: " + header[36:68].hex() + ", " + \
                                          "Time: " + time.strftime('%Y-%m-%d %H:%M:%S',
                                                       time.gmtime(int.from_bytes(header[68:72], byteorder='little'))) + ", " + \
                                          "nBits: " + str(int.from_bytes(header[72:76], byteorder='little')) + ", " + \
                                          "Nonce: " + str(int.from_bytes(header[76:80], byteorder='little'))
                        rest = payload[80:]
                        firstBit = rest[0]
                        iteration = compactSizeUintInterpreter(firstBit)
                        numberOfTransactions = int.from_bytes(rest[iteration[0]:iteration[1]], byteorder='little')
                        #print("Number of transactions:")
                        #print(numberOfTransactions)
                        payloadStr.append(headerStr)
                        payloadStr.append("Number of transactions: " + str(numberOfTransactions))
                        transactionBegin = iteration[1]
                        transactions = rest[transactionBegin:]
                        #transactionEnd = -1
                        iterator = 0
                        for i in range(numberOfTransactions):
                            version = str(int.from_bytes(transactions[transactionBegin:transactionBegin+4], byteorder='little'))
                            firstBit = transactionBegin+4
                            iteration = compactSizeUintInterpreter(firstBit)
                            tx_in_count = int.from_bytes(transactions[iteration[0]:iteration[1]], byteorder='little')
                            payloadStr.append("Transaction\n" +
                                              "Version: " + version + ", " +
                                              "Number of inputs: " + str(tx_in_count)
                                )
                            iterator = iteration[1]
                            for j in range(tx_in_count):
                                out_point_length = 36
                                hash = transactions[iterator:32].hex()
                                iterator += 32
                                index = str(int.from_bytes(transactions[iterator:4], byteorder='little'))
                                iterator += 4
                                payloadStr.append("Input")
                                payloadStr.append("Previous output - " +
                                                  "Hash: " + hash + ", "
                                                  "Index: " + index
                                                  )
                                firstBit = iterator
                                iteration = compactSizeUintInterpreter(firstBit)
                                script_bytes = int.from_bytes(transactions[iteration[0]:iteration[1]], byteorder='little')
                                iterator = iteration[1]
                                signature_script = transactions[iterator:iterator+script_bytes].hex()
                                iterator += script_bytes
                                sequence = str(int.from_bytes(transactions[iterator:iterator+4], byteorder='little'))
                                iterator += 4
                                payloadStr.append("Script bytes: " + str(script_bytes) + ", " + 
                                                  "Signature script: " + signature_script + ", " +
                                                  "Sequence: " + sequence
                                                  )
                            firstBit = iterator
                            iteration = compactSizeUintInterpreter(firstBit)
                            tx_out_count = int.from_bytes(transactions[iteration[0]:iteration[1]], byteorder='little')
                            payloadStr.append("Number of outputs: " + str(tx_out_count))
                            iterator = iteration[1]
                            for k in range(tx_out_count):
                                payloadStr.append("Output")
                                value = int.from_bytes(transactions[iterator:iterator+8], byteorder='little')
                                iterator += 8
                                firstBit = iterator
                                iteration = compactSizeUintInterpreter(firstBit)
                                pk_script_bytes = int.from_bytes(transactions[iteration[0]:iteration[1]], byteorder='little')
                                iterator = iteration[1]
                                pk_script = transactions[iterator:iterator+pk_script_bytes].hex()
                                iterator += pk_script_bytes
                                payloadStr.append("Value: " + str(value) + ", " +
                                                  "Bytes in the pubkey script: " + str(pk_script_bytes) + ", " +
                                                  "Pubkey script: " + pk_script
                                                  )
                        transactionBegin = iterator
                        globalNumberOfBlocks += 1
                        myBlocks.append(str(globalNumberOfBlocks) + ".")
                        myBlocks.extend(payloadStr)
                    else:
                        if(commandName == 'ping'):
                            payloadStr.append("Nonce: " + payload.hex())
                        #payloadStr.append("Cannot interpret this type of payload yet")
        

    return payloadStr

def getAddr(s, magic):
    commandName = "getaddr"
    command = commandName + (12 - len(commandName)) * "\00"
    payload = str.encode("")

    length = struct.pack("I", len(payload))

    check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]

    msg = magic + str.encode(command) + length + check + payload

    s.send(msg)
    response = s.recv(1024)
    print("getaddr respone \n")
    print("length")
    print(len(response))
    print("response")
    print(response)
    #version = struct.pack("i", 70015)
    #commandName = "getblocks"
    #command = commandName + (12 - len(commandName)) * "\00"
    #payload = str.encode("")
    #hash_count = struct.pack("I", 50)
    #length = struct.pack("I", len(payload))

    #check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]

    #msg = magic + str.encode(command) + length + check + payload

    #s.send(msg)
    #response = s.recv(1024)
    #print("getblocks respone \n")
    #print(response)


def sendMessageWithoutPayload(s, magic, commandName):
    command = commandName + (12 - len(commandName)) * "\00"
    payload = str.encode("")

    length = struct.pack("I", len(payload))

    check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]

    msg = magic + str.encode(command) + length + check + payload
    s.send(msg)


def getBlocks(s, magic, version, firstBlockHash):
    commandName = "getblocks"
    command = commandName + (12 - len(commandName)) * "\00"
    hashCount = struct.pack("B", 1) #for now. Now we send only one hash
    blockHeaderHashes = firstBlockHash #for now. Now we send only one hash
    stopHash = b'\0' * 32 #for now. Now we send only one hash
    payload = version + hashCount + blockHeaderHashes + stopHash
    length = struct.pack("I", len(payload))
    check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    msg = magic + str.encode(command) + length + check + payload
    s.send(msg)
    return


def getHeaders(s, magic, version, firstBlockHash):
    commandName = "getheaders"
    command = commandName + (12 - len(commandName)) * "\00"
    hashCount = struct.pack("B", 1) #for now. Now we send only one hash
    blockHeaderHashes = firstBlockHash #for now. Now we send only one hash
    stopHash = b'\0' * 32 #for now. Now we send only one hash
    payload = version + hashCount + blockHeaderHashes + stopHash
    length = struct.pack("I", len(payload))
    check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    msg = magic + str.encode(command) + length + check + payload
    s.send(msg)
    return

def intToCompactSizeInt(input):
    if(input > 252):
        inBytes = bytes.fromhex("fd")
        print("Int codec begin")
        print(inBytes)
        print("Input number in little endian bytes")
        print(struct.pack('<I', input))
        numberInBytes = struct.pack('<I', input)
        inBytes = inBytes + numberInBytes[:2]
        print("The final number in compactSizeInt")
        print(inBytes)
        return inBytes
    else:
        print("Input number in little endian bytes")
        print(struct.pack('<I', input))
        numberInBytes = struct.pack('<I', input)
        inBytes = numberInBytes[:1]
        print("The final number in compactSizeInt")
        print(inBytes)
        return inBytes

def getData(s, magic):
    commandName = "getdata"
    command = commandName + (12 - len(commandName)) * "\00"
    numberOfData = -1
    print("How many data do you want to get (500 max)?")
    numberOfData = input()
    payload = intToCompactSizeInt(int(numberOfData))
    print("Choose number of block-data")
    myBlockHeadersLen = len(myBlockHeadersHashes)
    for i in range(myBlockHeadersLen):
        print(str(i) + ". " + myBlockHeadersHashes[i])
    for i in range(int(numberOfData)):
        choise = input()
        payload = payload + myBlockHeadersHashesInBytes[int(choise)]
    length = struct.pack("I", len(payload))
    check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    msg = magic + str.encode(command) + length + check + payload
    s.send(msg)
    return numberOfData

def ping(s, magic, nonce):
    commandName = "ping"
    command = commandName + (12 - len(commandName)) * "\00"
    payload = nonce
    length = struct.pack("I", len(payload))
    check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    msg = magic + str.encode(command) + length + check + payload
    s.send(msg)
    return

def pong(s, magic, nonce):
    commandName = "pong"
    command = commandName + (12 - len(commandName)) * "\00"
    payload = nonce
    length = struct.pack("I", len(payload))
    check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    msg = magic + str.encode(command) + length + check + payload
    s.send(msg)
    print("pong send")
    return



genesisBlockHash = b'\00\00\00\00\00\19\d6\68\9c\08\5a\e1\65\83\1e\93\4f\f7\63\ae\46\a2\a6\c1\72\b3\f1\b6\0a\8c\e2\6f'
print("Genesis block hash")
print(genesisBlockHash)
version = struct.pack("i", 70015)
services = struct.pack("Q", 0) #Q means 8 bytes long integer
timestamp = struct.pack("q", int(time.time()))

addr_recv_services = struct.pack("Q", 0)
addr_recv_ip = struct.pack(">16s", bytes("52.144.47.153", 'utf-8')) #> - big endian, 16 - sixteen, s - characters
addr_recv_port = struct.pack(">H", 8333) #8333 is a default port for bitcoin protocol

addr_trans_services = struct.pack("Q", 0)
addr_trans_ip = struct.pack(">16s", bytes("127.0.0.1", 'utf-8')) #> - big endian, 16 - sixteen, s - characters
addr_trans_port = struct.pack(">H", 8333) #8333 is a default port for bitcoin protocol

nonce = struct.pack("Q", random.getrandbits(64))
user_agent_bytes = struct.pack("B", 0)
starting_height = struct.pack("i", 0)
relay = struct.pack("?", False)

payload = version + services + timestamp + addr_recv_services + addr_recv_ip + addr_recv_port + \
    addr_trans_services + addr_trans_ip + addr_trans_port + nonce + user_agent_bytes + starting_height + relay

magic = codecs.decode('F9BEB4D9', 'hex')
command = "version" + 5 * "\00" #\00 means null in python
length = struct.pack("I", len(payload))

check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]

msg = magic + str.encode(command) + length + check + payload

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#HOST = "158.129.212.251"
HOST = "dnsseed.bitcoin.dashjr.org"
PORT = 8333

s.connect((HOST, PORT))
s.send(msg)
response = readHeader(s)
print("version header response \n")
print("header response length \n")
print(len(response))
print(response.hex())
h = headerInterpreter(response)

payload = s.recv(h.length) #recive bytes length of payload
print("payload length \n")
print(len(payload))
print("payload")
print(payload)
peerVersion = massageClass()
peerVersion.header = h
peerVersion.payload = payload

#print("header of message")
#print(headerInterpreter(response))



commandName = "verack"
command = commandName + (12 - len(commandName)) * "\00"
payload = str.encode("")

check = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
length = struct.pack("I", len(payload))
msg = magic + str.encode(command) + length + check + payload

s.send(msg)

response = readHeader(s)
print("verack header response \n")
print("header verack length \n")
print(len(response))
print(response.hex())
h = headerInterpreter(response)

payload = s.recv(h.length) #recive bytes length of payload
print("payload length \n")
print(len(payload))
print("payload")
print(payload)
peerVerack = massageClass()
peerVerack.header = h
peerVerack.payload = payload
for i in range(6): #pobierzmy 6 pierwszych wiadomości, które są zawsze wysyłane. Jedna z nich to ping, na który prawdopodobnie trzeba odpowiedzieć
    response = readHeader(s)
    #print("response header length")
    #print(len(response))
    #print("response header")
    #print(response.hex())
    h = headerInterpreter(response)
    payload = s.recv(h.length) #recive bytes length of payload
    #print("payload length")
    #print(len(payload))
    #print("payload")
    #print(payload.hex())
    payloadStr = payloadInterpreter(payload, h.command)
    #print("payload interpreted")
    for i in payloadStr:
            print(i)
        #nonce = payloa
    if h.command == 'ping':
        pong(s, magic, payload)

while True:
    print("1. getaddr\n2. getblocks\n3. getheaders\n4. getdata\n5. ping \n6. My block headers hashes\n7. My block headers\n8. My blocks")
    choise = input()
    commandName = ''
    numberOfData = 1
    if(choise == '1'):
        commandName = 'getaddr'
        sendMessageWithoutPayload(s, magic, commandName)
    if(choise == '2'):
        getBlocks(s, magic, version, genesisBlockHash)
    if(choise == '3'):
        getHeaders(s, magic, version, genesisBlockHash)
    if(choise == '4'):
        numberOfData = getData(s, magic)
    if(choise == '5'):
        ping(s, magic, np.random.bytes(8))
    if(choise == '6'):
        for i in myBlockHeadersHashes:
            print(i)
    if(choise == '7'):
        for i in myBlockHeaders:
            print(i)
    if(choise == '8'):
        for i in myBlocks:
            print(i)
    if(choise != '6' and choise != '7' and choise != '8'):
        intNumberOfData = int(numberOfData)
        j = 0
        while(j < intNumberOfData):
            response = readHeader(s)
            print("response header length")
            print(len(response))
            print("response header")
            print(response.hex())
            h = headerInterpreter(response)
            payload = s.recv(h.length) #recive bytes length of payload
            if(len(payload) > h.length):
                print("Error: Recieved too much payload")
            if(len(payload) < h.length):
                print("Recieved not enought payload")
                print("Payload before addition")
                print(payload)
                while len(payload) < h.length :
                    numberOfBytesLeft = h.length - len(payload)
                    addition = s.recv(numberOfBytesLeft)
                    #additionInArray = list(addition)
                    #payloadInArray = list(payload)
                    #payloadInArray.append(additionInArray)
                    #payload = bytes(payloadInArray)
                    additionInString = addition.hex()
                    payloadInString = payload.hex()
                    payloadInString += additionInString
                    payload = bytes.fromhex(payloadInString)
                    print("Payload length after addition")
                    print(len(payload))
                    print("Payload after addition")
                    print(payload)
            #print("payload length")
            #print(len(payload))
            #print("payload")
            #print(payload.hex())
            payload = payloadInterpreter(payload, h.command)
            print("payload")
            for i in payload:
                    print(i)
            if(h.command == 'inv'):
                    myBlockHeadersHashes = payload[1:]
            if(choise == '1' and h.command != 'addr' or
               choise == '2' and h.command != 'inv' or
               choise == '3' and h.command != 'headers' or
               choise == '5' and h.command != 'pong'):
                print("Another type of message expected")
                j = j - 1
            if(choise == '4'):
                correctAnswer = False
                if(h.command == 'tx' or
                   h.command == 'block' or
                   h.command == 'merkleblock' or
                   h.command == 'cmpctblock' or
                   h.command == 'notfound'):
                    correctAnswer = True
                if correctAnswer == False :
                    print("Another type of message expected")
                    j = j - 1
            j = j + 1


#response = s.recv(1024)
#print("verack respone \n")
#print("length")
#print(len(response))
#print("response")
#print(response.hex())
#print("\n")

#getAddr(s, magic)

#while True:
#    a = input()
#    if(a == "1"):
#        getAddr(s, magic)
#    response = s.recv(1024)
#    print("response")
#    print(response)
